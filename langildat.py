###################################################
####    REPLICATE STASTISTICAL OUTPUT            18 Aug '15
###################################################
# Data loading, statistical analysis and reformating from gillespie.cpp data
# * loadtrajectories() reads all the trajectory files and its input files into a dictionary called pointlist
#     it also creates a file called averlist with information on the replicate averages and deviations

# Once pointlist and averlist are loaded, the rest of the functions become avaiable:
#  * getSlist() returns a list with the used Signals
#  * getGTfromtraj() creates a matrix from averlist giving the average expression (or deviation) at certain signal
#  and time values X(G,T) which is useful to plot spatial profiles. It also gives the time and signal arrays
# used. It can sabe all this info in separate files that can be loaded in future sessions without the need of passing 
# through every single trajectory again (good when lots of stochastic replicates are analysed). The function to 
# read these files is:
#  *getGTfromfile()

# The signal is still file dependent, A better implementation is in order

import numpy
import re
import os
import numpy as np
from scipy.optimize import root


#### AUX FUNCTIONS
###################################
def getdict(filename): #  create a dictionary from the input file
	d={}
	for line in open(filename):
		li=line.strip()
#		print li
		if (li and not li.startswith("#")) : # if the line has soemthing and does not start with a #
			lineinfo= line.split()
			d[lineinfo[0]]=lineinfo[1]
	return d

def greaterthan(x,threshold): # this functions are useful for the Mean First Passage calculation
	return all([xx>threshold for xx in x]) # gives positive if all elements of x are greater than the threshold
	# note that is its necessary for x to be an array

def smallerthan(x,threshold): #ditto to greaterthan
	return all([xx<threshold for xx in x])

################################################
def updateaverlist(averlist,d,A,basename,actfrac=False):
# averlist keeps track of average in replicates, when comparing 
	for ipt,pt in enumerate(averlist):
		if all ([pt[key]==d[key] for key in d.keys()]): # Note that the only keys compared at this point don't include replicate number
			averlist[ipt]["sumexp"]=averlist[ipt]["sumexp"]+A[:,1:-1] # first column is time, we don't need it
			averlist[ipt]["sumexp2"]=averlist[ipt]["sumexp2"]+np.power(A[:,1:-1],2)
			averlist[ipt]["sumsignal"]=averlist[ipt]["sumsignal"]+A[:,-1]
			averlist[ipt]["sumsignal2"]=averlist[ipt]["sumsignal2"]+np.power(A[:,-1],2)
			averlist[ipt]["N"]=averlist[ipt]["N"]+1
			averlist[ipt]["files"].append(basename)
			if actfrac:
				af=[0 if x>0.5*averlistp[ipt]["Omega"] else 1 for x in A[:,2]]
				# This computes a certain activation threshold, it has to be changed manually so far
				averlist[ipt]["actfrac"]=averlist[ipt]["actfrac"]+af
			return True # The update returns to the original call
	# If it has not returned at this point, a new entry to averlist is created
	daver=dict(d)
	daver.update({"time":A[:,0],"sumexp":A[:,1:-1],"sumexp2":np.power(A[:,1:-1],2)})
	daver.update({"sumsignal":A[:,-1],"sumsignal2":np.power(A[:,-1],2)})
	daver.update({"N":1}) 
	daver.update({"files":[basename]})
	if actfrac:
		af=[0 if x>0.5*averlistp[ipt]["Omega"] else 1 for x in A[:,2]]
		daver.update({"actfrac":af})
	averlist.append(daver)
	return False

########## LOAD TRAJECTORIES FROM OUTPUT FILES
######################################### 
def loadtrajectories(foldername,regexp,rest={}):
	print "Loading files..."
	pointlist=[] # list with the info of all the files
	averlist=[] # list with the statistical average of replicates
	for file in os.listdir(foldername):
		if file[-4:]==".out": # Only if a related output file exists
			d={}
			basename=file[:-4] # Basename is taken
#			print basename
			d=getdict(foldername+"/"+basename+".in")
			if all([d[key]==rest[key] for key in rest.keys()]): # rest contains the restiction to keep on loading
#				A=np.loadtxt(foldername+"/"+basename+".out")
				A=np.fromfile(foldername+"/"+basename+".out",dtype=np.float32,sep=" ")
				A=np.reshape(A,(-1,6))
				updateaverlist(averlist,d,A,basename) # before increasing dictioanry d, it is used to update averlist
				g=regexp.search(basename) # It can be regular-expression analized
				try:
					title=g.groups()
					d["filename"]=basename 
					d["Ncell"]=int(title[-1]) # The replicate number is saved 
					d["expr"]=A[:,1:-1] # Expression matrix
					d["time"]=A[:,0] # time vector
					d["signal"]=A[:,-1] # signal vector (not this is not global to allow noisy signals)
					pointlist.append(dict(d))
				except:
					pass
	print str(len(pointlist))+" different files loaded into pointlist"
	return pointlist,averlist

########## LOAD MFPT FROM OUTPUT FILES
######################################### 
# This script loads the data from MFPT obtained from getTransition() - without info on trajectories
def loadMFPTs(foldername,regexp,rest={},successratiothreshold=0.9999):
	print "Loading files..."
	averlist=[] # list with the statistical average of replicates, we don't care anymore on pointlist
	for file in os.listdir(foldername):
		if file[-4:]==".out": # Only if a related output file exists
			d={}
			basename=file[:-4] # Basename is taken
			print basename
			d=getdict(foldername+"/"+basename+".in")
			if all([d[key]==rest[key] for key in rest.keys()]): # rest contains the restiction to keep on loading
				#T=np.fromfile(foldername+"/"+basename+".out",dtype=np.float32,sep=" ")
				T=np.loadtxt(foldername+"/"+basename+".out",comments="#") # This function is a bit slower than np.fromfile() but is able to skip comment lines
				# it may be useful to remove the comments from the files if there are lots of them and use fromfile() instead
				T=np.reshape(T,(-1,3))
				if len(T)>0:
					print basename
					g=regexp.search(basename) # It can be regular-expression analized
					title=g.groups()
					d["filename"]=basename 
					d["Ncells"]=len(T) # The replicate number is saved 
					successtimes=[t[0] for t in T if t[2]==0] # only trajectories that reached the final mean state are counted
					d["times"]=successtimes #
					d["successratio"]=len(d["times"])/(1.0*d["Ncells"]) # number of trajectories that finished on time, can be used to set a threshold to accept result
					print d["successratio"]
					if d["successratio"]<successratiothreshold: # if more than half of the points failed, the measure is not going to be good
						d["MFPTaver"],d["MFPTsigma"],d["MFPTerror"]=np.NaN,np.NaN,np.NaN #  if there is no successratio big enough
					elif d["successratio"]>0.9: # If all the points are accepted the statistics as easy
						d["MFPTaver"],d["MFPTsigma"],d["MFPTerror"]=np.mean(successtimes),np.std(successtimes),np.std(successtimes)/np.sqrt(d["Ncells"])
					else:
						# When some of the jumps didn't occur the statistics can still be inferred using a biased exponential distribution under "totaltime"
						d["MFPTaver"],d["MFPTsigma"],d["MFPTerror"]=biasedMFPT(successtimes,float(d["totaltime"])) # 
					print d["successratio"],d["MFPTaver"],d["MFPTsigma"],d["MFPTerror"],file
					averlist.append(dict(d))
	print str(len(averlist))+" different files loaded into averlist"
	return averlist

###################################### ORDERED GLIST (STRINS)
def getSlist(pointlist,rest={}): # it can be averlist as well
	Slist=[]
	for point in pointlist:
		if all ([point[key]==rest[key] for key in rest.keys()]):
			if (point["S"]) not in Slist:
				Slist.append(point["S"])
	Slist=sorted(Slist,key=float) # transforms strings to float to sort but does not cast the final array
	return Slist

##################################### MATRIXES EXPRESSION(G,T) FOR USE IN XPROFILES
def setGTdict(averdict,savefile=False):
	GTdict={}
	if savefile:
		with open(savefile,"a") as f:
			for key in averdict:
				if (key not in ["S","sumexp","sumexp2","sumsignal","sumsignal2","time"]):
					f.write(key+" "+str(averdict[key])+"\n")
				GTdict[key]=averdict[key]
	else:
		for key in averdict:
				if (key not in ["S","sumexp","sumexp2","sumsignal","sumsignal2","time"]):
					GTdict[key]=averdict[key]
	return GTdict

###################################
def getGTfromtraj(averlist,foldername,savefile=False,tini=0,tend=-1,tstep=1,rest={},coor=1,coorname="X"):
# Sets the global slices Xt, Xt2 --> first and second momenta concentration expressions for the desired time points info form of matrixes (G,T)
# Same thing for signal in time Gt ang Gt2

	Slist=getSlist(averlist)
	if tend==-1 :
		tend=len(averlist[0]["time"]) # if tend=-1 set it as the last element of time array
	timevalues=averlist[0]["time"][:]

	Xt=np.zeros(shape=(len(timevalues),len(Slist))) # G x T array
	Xt2=np.zeros(shape=(len(timevalues),len(Slist))) # G x T array
	Gt=np.zeros(shape=(len(timevalues),len(Slist))) # G x T array
	Gt2=np.zeros(shape=(len(timevalues),len(Slist))) # G x T array
	print "fromtraj shape at the beginning"+str(Xt.shape)
	# Gt and Gt2 contain the first and second momenta of the signal (useful for noisy signals)
	# Xt and Xt2 contain the first and second momenta of expressions
	for iG,G in enumerate(Slist):
		restdict={"S":G} #  First restriction is coincidence with target Gli
#		restdict.update({"DG":adaptmethod[0]})
		restdict.update(rest) #  Rest of restrictions
		a1 = [a for a in averlist if all ([a[key]==restdict[key] for key in restdict.keys()])]
		if len(a1)!=1: #  Catch if there are not enough restrictions
			print "Not enough restrictions. "+str(len(a1))+" different possibilities"
			for a in a1:
				print a["files"]
			return 
		Xt[:,iG]=a1[0]["sumexp"][:,coor]/a1[0]["N"]/float(a1[0]["Omega"])
		Xt2[:,iG]=a1[0]["sumexp2"][:,coor]/a1[0]["N"]/float(a1[0]["Omega"])/float(a1[0]["Omega"])
		Gt[:,iG]=a1[0]["sumsignal"][:]/a1[0]["N"]
		Gt2[:,iG]=a1[0]["sumsignal2"][:]/a1[0]["N"]
	print "getGRfromtraj",Xt
	GTdict=setGTdict(a1[0])
	if savefile:
		filenameGT=""
		for key in rest:
			filenameGT=filenameGT+key+rest[key]
		print "Writing files... "+foldername+"/"+filenameGT+coorname+"*"
		np.savetxt(foldername+"/"+filenameGT+coorname+"t"+".dat",Xt)
		np.savetxt(foldername+"/"+filenameGT+coorname+"t2"+".dat",Xt2)
		np.savetxt(foldername+"/"+filenameGT+"S"+"t"+".dat",Gt)
		np.savetxt(foldername+"/"+filenameGT+"S"+"t2"+".dat",Gt2)
		np.savetxt(foldername+"/"+filenameGT+"times"+".dat",timevalues)
		np.savetxt(foldername+"/"+filenameGT+"Slist"+".dat",[float(x) for x in Slist])
		GTdict=setGTdict(averlist[0],foldername+"/"+filenameGT+"dict"+".dat")
	print "fromtraj shape at the end"+str(Xt.shape)
	return Xt,Xt2,Gt,Gt2,timevalues,Slist,GTdict

#####################################################
def getGTfromfile(folder,rest,coorname="X",tini=0,tend=-1,tstep=1):
# Sets GT from a file created previous run of getGTfromtraj 
	filenameGT=""
	for key in rest:
		filenameGT=filenameGT+key+rest[key]
	Xt=np.fromfile(folder+"/"+filenameGT+coorname+"t"+".dat",dtype=np.float32,sep=" ")
	Xt2=np.fromfile(folder+"/"+filenameGT+coorname+"t2"+".dat",dtype=np.float32,sep=" ")
	Gt=np.fromfile(folder+"/"+filenameGT+"S"+"t"+".dat",dtype=np.float32,sep=" ")
	Gt2=np.fromfile(folder+"/"+filenameGT+"S"+"t2"+".dat",dtype=np.float32,sep=" ")
	Tlist=np.fromfile(folder+"/"+filenameGT+"times"+".dat",dtype=np.float32,sep=" ")
	Slist=np.fromfile(folder+"/"+filenameGT+"Slist"+".dat",dtype=np.float32,sep=" ")
	Gtdict=getdict(folder+"/"+filenameGT+"dict"+".dat")

	Xt=np.reshape(Xt,(-1,len(Slist)))
	Xt2=np.reshape(Xt2,(-1,len(Slist)))
	Gt=np.reshape(Gt,(-1,len(Slist)))
	Gt2=np.reshape(Gt2,(-1,len(Slist)))

	# Xt=Xt[tini:tend:tstep,:] # 
	# Xt2=Xt2[tini:tend:tstep,:]
	# Gt=Gt[tini:tend:tstep,:]
	# Gt2=Gt2[tini:tend:tstep,:]
	# Tlist=Tlist[tini:tend:tstep]
	print "GT shape is"+str(Xt.shape)

	return Xt,Xt2,Gt,Gt2,Tlist,Slist,Gtdict

########################################################
def setMFPT(pointlist,averlist=False,coor=[0],conditions=[greaterthan],threshold=[0.9],window=1,rest={}):
# lists contains the conditions (gp.greatherthan or gp.smallerthan) and their arguments, but new functions
# can be defined
# window check that the condition is fulfilled on a span window of times to avoid fluctuating MFPT

	if not averlist and not pointlist:
		print "No data loaded!"
	if averlist and not pointlist:
		print "Using MFPT values directly from files"
	elif averlist: # if averlist is found is subsequently updated to have <MFPT> and <MFPT^2>
		for iptaver,ptaver in enumerate(averlist): # resetting (or initializaing) average values
			averlist[iptaver]["sumMFPT"]=0
			averlist[iptaver]["sumMFPT2"]=0

	for ipoint,point in enumerate(pointlist): # loop to compute MFPT on every single trajectory
		if all ([point[key]==rest[key] for key in rest.keys()]):
			for irow,row in enumerate(point["expr"]): # Each row is searched in time for the conditions to apply
				if irow<len(point["expr"])-(window-1): # avoid windowing past last row
					iwindows=np.arange(irow,irow+window)# remember that arange does not include last element
					if all([cond([point["expr"][ir,coor[icond]]/float(point["Omega"])], threshold[icond]) 
						for icond,cond in enumerate(conditions) for ir in iwindows]):
				# for every row in the window range (iwindows) check that each one of the coordinates coor
				# follow conditions with the determined thresholds		
				# when all the conditions are met, get the value and break the loop
						timeMFPT=point["time"][irow] # MFPT found
						break
			else: # This else belongs to the loop and triggers when it finishes without the break (no MFPT found)
				timeMFPT=float("inf")# Set it to infinity 			
			pointlist[ipoint].update({"MFPT":timeMFPT}) 

			if averlist:
				for iptaver,ptaver in enumerate(averlist): # look for the averlist correspondence of point
					exceptkeys=["Ncell","expr","filename","signal","time","MFPT"] # compare replicates w/o their differences
					if all ([ptaver[key]==point[key] for key in point.keys() if key not in exceptkeys]): 
							averlist[iptaver]["sumMFPT"]=averlist[iptaver]["sumMFPT"]+timeMFPT 
							averlist[iptaver]["sumMFPT2"]=averlist[iptaver]["sumMFPT2"]+timeMFPT*timeMFPT 
							break # once a match is found, the aver loop is ended to avoid extra calculations
	if averlist: # transformations of sums in averages
		for iav,av in enumerate(averlist):
			sum1=averlist[iav]["sumMFPT"]
			sum2=averlist[iav]["sumMFPT2"]
			Ns=averlist[iav]["N"]
			avelist[iav]["MFPTaver"]=sum1/Ns
			averlist[iav]["MFPTsigma"]=np.sqrt(sum2/Ns-(sum1/N)*(sum1/Ns))
			averlist[iav]["MFPTerror"]=averlist[iav]["MFPTsigma"]/np.sqrt(Ns)

###########################################################
# This functions approximates the average rate of exponentially distributed times that are computed until a certain threshold.
# This is useful to compute MFPT of rare events avoiding to compute all the jumps (ignoring the tails)
# The resulting MFPT (inverse of the rate) is given by the root of the function biasedexpT() computed in biasedMFT() details are given in pp100 of nb alpha
# So far the algorithm only includes measures based on average accepted times but there is also information in the amount of events that didn't arrive
# prior to the threshold. More info and proposed formulae are in pp 103,104

def biasedexpT(k,threshold,measuredtime):
	return threshold*(np.exp(-1.0*k*threshold))/(np.exp(-1.0*k*threshold)-1.0)+1.0/k-measuredtime

def biasedMFPT(Tlist,threshold): 
	measuredtime=np.mean(Tlist)
	sigmaT=np.std(Tlist)
	kopt=root(biasedexpT,1/measuredtime,args=[threshold,measuredtime],method="broyden1") # initial guess is the actual rate ignoring the threshold
	kopt=kopt.x # unpacking of root result
	predictedsigma=np.sqrt(sigmaT*sigmaT+threshold*threshold*np.exp(-1.0*kopt*threshold)/np.power(1-np.exp(-1.0*kopt*threshold),2)) # this is done by seeing that vartotal=varbiased+somefunction(k,xa), it is not optimal though
	errorA=sigmaT/np.sqrt(len(Tlist)) # error of the biased distribution
	error=errorA/(1+(measuredtime-1.0/kopt)*kopt*kopt*(threshold-measuredtime+1.0/kopt)) # old computation on sigma based on error original error of the mean and propagating 
	# It may be worth to rewrite the error by calculating the expected MFPT for the calculated k+-its error
#	return 1.0/kopt,predictedsigma,error #  returns T, sigma(T), error of prediction

	
