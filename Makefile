
# COMPILACIÓN DE MAPmin

CFLAGS = -DNDEBUG -O -std=c++11 -Wall -msse2 # <-- This path includes the dlib library
LFLAGS = -lm -lgsl -lgslcblas
GPP = g++
OBJECTS = langil.o HIVlangil.o 

#Linking
HIVlangil: $(OBJECTS)
	$(GPP) $(CFLAGS) -o HIVlangil $(OBJECTS) $(LFLAGS)

HIVlangil.o: HIVlangil.cpp
	$(GPP) $(CFLAGS) -c HIVlangil.cpp

langil.o: langil.cpp langil.h
	$(GPP) $(CFLAGS) -c langil.cpp
